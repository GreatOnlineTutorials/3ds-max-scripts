a.rotation.controller=lookat_constraint()

g=a.rotation.controller

g.appendtarget $sphere001 50

g.lookat_vector_length=50

g.viewline_length_abs=off

g.set_orientation=off

g.relative=off

g.target_axis=2

g.target_axis=0

g.target_axisflip=on

y=sphere radius:5 pos:[0,-20,0]

g.appendtarget $sphere002 50

g.getnumtargets()

g.getweight 1

g.setweight 1 100

g.setweight 2 0

g.deletetarget 2

