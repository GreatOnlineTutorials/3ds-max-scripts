rollout chanrao "沿线随机缠绕" width:207 height:619
(
	label lbl1 "沿线随机缠绕插件 v1.0" pos:[42,15] width:123 height:20
	pickbutton fenbuyangtiaoxian "拾取分布样条线" pos:[50,38] width:105 height:22
	button jinchanrao "仅缠绕" pos:[50,317] width:101 height:23
	spinner wutishumu "物体数目" pos:[24,74] width:115 height:16 range:[0,10000,20] type:#integer
	spinner wuticuxi "物体粗细" pos:[24,102] width:115 height:16 enabled:true range:[0,100,1] type:#float
	spinner wutiduanshu "物体段数" pos:[24,131] width:115 height:16 enabled:true range:[0,10000,20] type:#integer
	spinner wutichangdu "物体长度" pos:[24,161] width:115 height:16 enabled:true range:[0,10000,20] type:#integer
	button cuxigengxin "更新" pos:[155,101] width:39 height:18
	button duanshugengxin "更新" pos:[155,130] width:39 height:18
	button changdugengxin "更新" pos:[155,159] width:39 height:18
	button chongxinfenbu "重新分布" pos:[50,353] width:101 height:23
	dropdownList wutileixing "物体类型" pos:[52,255] width:105 height:41 items:#("圆柱体", "立方体", "自定义") selection:0
	pickbutton zidingyiwuti "拾取自定义物体" pos:[50,389] width:101 height:23
	spinner donghuashichang "动画时长" pos:[24,190] width:115 height:16 enabled:true range:[0,10000,100] type:#integer
	spinner wutixuanzhuan "物体旋转" pos:[24,220] width:115 height:16 enabled:true range:[0,10000,20] type:#integer
	button btn29 "更新" pos:[155,220] width:39 height:18
	button anxianxuanzewuti "按线选择物体" pos:[49,426] width:101 height:23
	colorPicker shiquyanse "拾取颜色" pos:[48,462] width:103 height:25
	button zhengtiyansesuiji "整体颜色随机" pos:[49,502] width:101 height:23
	button anxianyansesuiji "按线颜色随机" pos:[49,539] width:101 height:23
	button anxiansuijixianshi "按线随机显示" pos:[49,576] width:101 height:23
	on fenbuyangtiaoxian picked obj do
	(
			fenbuyangtiaoxian.text=obj.name
			obj.name="quxian"
			deselect objects
	)
	on jinchanrao pressed do
	(
	  if wutileixing.selected=="圆柱体" then for i=1 to wutishumu.value do cylinder radius:(random ((wuticuxi.value)*0.5) ((wuticuxi.value)*1.5)) height:(random ((wutichangdu.value)*0.2) ((wutichangdu.value)*1.5)) heightsegs:(wutiduanshu.value) pos:[5000,5000,5000] prefix:"wuti"
	  if wutileixing.selected=="立方体" then for i=1 to wutishumu.value do box length:(random ((wuticuxi.value)*0.5) ((wuticuxi.value)*1.5)) width:(random ((wuticuxi.value)*0.5) ((wuticuxi.value)*1.5)) height:(random ((wutichangdu.value)*0.2) ((wutichangdu.value)*1.5)) heightsegs:(wutiduanshu.value) pos:[5000,5000,5000] prefix:"wuti"
	  if wutileixing.selected=="自定义" then for i=1 to wutishumu.value do copy $zidingyi pos:[5000,5000,5000] prefix:"wuti"
	  select $wuti*
	  global c= selection as array
	  for a in c do addmodifier a (spacepathdeform path:$quxian)
	  for a in c do a.transform=$quxian.transform
	  animationrange=interval 0 donghuashichang.value
	  animate on at time donghuashichang.value for a in c do a.path_deform_binding.percent_along_path=100
	  select c
	  for a in c do setbeforeort a.path_deform_binding.percent_along_path.controller #loop
	  for a in c do setafterort a.path_deform_binding.percent_along_path.controller #loop
	  for a in c do a.path_deform_binding.percent_along_path=(random 0 100)
	  for a in c do a.path_deform_binding.revolutions=(random (0.5*wutixuanzhuan.value) (2*wutixuanzhuan.value))
	  for i=1 to ((c.count)*1/2) do reversetime c[random 1 (c.count)].path_deform_binding.percent_along_path.controller -1 ((donghuashichang.value)+1)
	  for i=1 to ((c.count)*1/2) do scaletime c[random 1 (c.count)].path_deform_binding.percent_along_path.controller 0 donghuashichang.value (random 0.0 1.0)
	)
	on cuxigengxin pressed do
		for a in c do a.radius=(random ((wuticuxi.value)*0.5) ((wuticuxi.value)*1.5))
	on duanshugengxin pressed do
		for a in c do a.heightsegs=(wutiduanshu.value)
	on changdugengxin pressed do
		for a in c do a.height=(random ((wutichangdu.value)*0.2) ((wutichangdu.value)*1.5))
	on chongxinfenbu pressed do
		delete geometry
	on zidingyiwuti picked obj do
	(
	  zidingyiwuti.text=obj.name
	  obj.name="zidingyi"
	  deselect objects
	)
	on btn29 pressed do
		for a in c do a.path_deform_binding.revolutions=(random (0.5*wutixuanzhuan.value) (2*wutixuanzhuan.value))
	on anxianxuanzewuti pressed do
	(
	  m=$
	  for a in geometry do
	  (
	  if classof a.modifiers[1]==spacepathdeform and a.path_deform_binding.path==m do selectmore a
	  )
	  global g=selection as array
	)
	on zhengtiyansesuiji pressed do
	(
	   for a in geometry do
	   (
	    if classof a.modifiers[1]==spacepathdeform do selectmore a
	   )
	   animate on at time 0 for a in selection do a.material=standard diffuse:(random black white)
	   animate on at time donghuashichang.value for a in selection do a.material.diffuse=(shiquyanse.color)
	)
	on anxianyansesuiji pressed do
	(
	   animate on at time 0 for a in selection do a.material=standard diffuse:(color ((shiquyanse.color.r)*(random 0.4 1.6)) ((shiquyanse.color.g)*(random 0.4 1.6)) ((shiquyanse.color.b)*(random 0.4 1.6)))
	   animate on at time donghuashichang.value for a in selection do a.material.diffuse=(shiquyanse.color)
	   deselect objects
	)
	on anxiansuijixianshi pressed do
	(
	   for a in g do
	   (
	      n=(donghuashichang.value/g.count)
	      for t=1 to (g.count) do
	      (
	         animate on at time ((t-1)*n) g[t].visibility=off
	         animate on at time (t*n) g[t].visibility=on
	      )
	   )
	   deselect objects
	)
)
createdialog chanrao
