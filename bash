#!/bin/bash
set -e

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_界面介绍 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm01_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm01_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_脚本基础01 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm01_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm01_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="03_脚本基础02 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm01_03.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm01_03.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="04_数学计算 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm01_04.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm01_04.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="05_常用判断语句 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm01_05.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm01_05.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="06_入门案例 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm01_06.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm01_06.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_基本创建 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm02_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm02_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_材质和可见性 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm02_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm02_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="03_批量创建 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm02_03.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm02_03.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_基本变换操作 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm03.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm03.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_选择和拷贝 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm04.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm04.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_技术详解01 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm05_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm05_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_技术详解02 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm05_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm05_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_技术详解01 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm06_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm06_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_技术详解02 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm06_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm06_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_修改器脚本 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm07.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm07.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_灯光脚本 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm08.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm08.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_摄影机脚本 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm09.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm09.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_位置控制器 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm10_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm10_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_旋转控制器 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm10_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm10_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="03_缩放控制器 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm10_03.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm10_03.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="04_链接约束 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm10_04.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm10_04.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_综合案例 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm11.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm11.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_新建脚本界面 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm12.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm12.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_界面介绍 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm13_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm13_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_位图和按钮 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm13_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm13_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="03_贴图和材质 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm13_03.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm13_03.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="04_拾取复选和颜色 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm13_04.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm13_04.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="05_组合框 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm13_05.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm13_05.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="06_下拉列表 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm13_06.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm13_06.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="07_列表框 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm13_07.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm13_07.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="08_编辑框 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm13_08.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm13_08.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="09_标签 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm13_09.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm13_09.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="10_组框和复选框 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm13_10.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm13_10.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="11_单选 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm13_11.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm13_11.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="12_微调器01 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm13_12.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm13_12.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="13_微调器02 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm13_13.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm13_13.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="14_进度条 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm13_14.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm13_14.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="15_滑块 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm13_15.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm13_15.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="16_计时器 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm13_16.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm13_16.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="17_ActiveX控件 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm13_17.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm13_17.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第1篇 脚本入门 - 第1篇 脚本入门 - 第1篇 脚本入门\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_自定义物体属性 [第1篇 脚本入门] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/rm14.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/rm14.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第2篇 动画提速 - 第2篇 动画提速 - 第2篇 动画提速\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_立体构成 [第2篇 动画提速] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/dh01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/dh01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第2篇 动画提速 - 第2篇 动画提速 - 第2篇 动画提速\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_步骤01 [第2篇 动画提速] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/dh02_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/dh02_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第2篇 动画提速 - 第2篇 动画提速 - 第2篇 动画提速\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_步骤02 [第2篇 动画提速] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/dh02_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/dh02_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第2篇 动画提速 - 第2篇 动画提速 - 第2篇 动画提速\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_步骤01 [第2篇 动画提速] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/dh03_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/dh03_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第2篇 动画提速 - 第2篇 动画提速 - 第2篇 动画提速\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_步骤02 [第2篇 动画提速] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/dh03_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/dh03_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第2篇 动画提速 - 第2篇 动画提速 - 第2篇 动画提速\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_步骤01 [第2篇 动画提速] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/dh04_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/dh04_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第2篇 动画提速 - 第2篇 动画提速 - 第2篇 动画提速\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_步骤02 [第2篇 动画提速] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/dh04_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/dh04_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第2篇 动画提速 - 第2篇 动画提速 - 第2篇 动画提速\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="03_步骤03 [第2篇 动画提速] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/dh04_03.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/dh04_03.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第2篇 动画提速 - 第2篇 动画提速 - 第2篇 动画提速\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_旋转波浪 [第2篇 动画提速] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/dh05.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/dh05.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第2篇 动画提速 - 第2篇 动画提速 - 第2篇 动画提速\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_步骤01 [第2篇 动画提速] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/dh06_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/dh06_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第2篇 动画提速 - 第2篇 动画提速 - 第2篇 动画提速\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_步骤02 [第2篇 动画提速] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/dh06_02.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/dh06_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第2篇 动画提速 - 第2篇 动画提速 - 第2篇 动画提速\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_步骤01 [第2篇 动画提速] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/dh07_01.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/dh07_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第2篇 动画提速 - 第2篇 动画提速 - 第2篇 动画提速\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_步骤02 [第2篇 动画提速] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/dh07_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/dh07_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第2篇 动画提速 - 第2篇 动画提速 - 第2篇 动画提速\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_步骤01 [第2篇 动画提速] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/dh08_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/dh08_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第2篇 动画提速 - 第2篇 动画提速 - 第2篇 动画提速\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_步骤02 [第2篇 动画提速] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD1/right/video/dh08_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/dh08_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第3篇 粒子控制 - 第3篇 粒子控制 - 第3篇 粒子控制\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_步骤01 [第3篇 粒子控制] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/lz01_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/lz01_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第3篇 粒子控制 - 第3篇 粒子控制 - 第3篇 粒子控制\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_步骤02 [第3篇 粒子控制] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/lz01_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/lz01_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第3篇 粒子控制 - 第3篇 粒子控制 - 第3篇 粒子控制\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_步骤01 [第3篇 粒子控制] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/lz02_01.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/lz02_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第3篇 粒子控制 - 第3篇 粒子控制 - 第3篇 粒子控制\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_步骤02 [第3篇 粒子控制] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/lz02_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/lz02_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第3篇 粒子控制 - 第3篇 粒子控制 - 第3篇 粒子控制\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="03_步骤03 [第3篇 粒子控制] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/lz02_03.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/lz02_03.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第3篇 粒子控制 - 第3篇 粒子控制 - 第3篇 粒子控制\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_步骤01 [第3篇 粒子控制] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/lz03_01.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/lz03_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第3篇 粒子控制 - 第3篇 粒子控制 - 第3篇 粒子控制\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_步骤02 [第3篇 粒子控制] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/lz03_02.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/lz03_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第3篇 粒子控制 - 第3篇 粒子控制 - 第3篇 粒子控制\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="03_步骤03 [第3篇 粒子控制] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/lz03_03.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/lz03_03.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第3篇 粒子控制 - 第3篇 粒子控制 - 第3篇 粒子控制\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_步骤01 [第3篇 粒子控制] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/lz04_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/lz04_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第3篇 粒子控制 - 第3篇 粒子控制 - 第3篇 粒子控制\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_步骤02 [第3篇 粒子控制] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/lz04_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/lz04_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第3篇 粒子控制 - 第3篇 粒子控制 - 第3篇 粒子控制\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="03_步骤03 [第3篇 粒子控制] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/lz04_03.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/lz04_03.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第3篇 粒子控制 - 第3篇 粒子控制 - 第3篇 粒子控制\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_步骤01 [第3篇 粒子控制] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/lz05_01.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/lz05_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第3篇 粒子控制 - 第3篇 粒子控制 - 第3篇 粒子控制\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_步骤02 [第3篇 粒子控制] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/lz05_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/lz05_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第3篇 粒子控制 - 第3篇 粒子控制 - 第3篇 粒子控制\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_乐透开奖 [第3篇 粒子控制] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/lz06.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/lz06.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第3篇 粒子控制 - 第3篇 粒子控制 - 第3篇 粒子控制\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_步骤01 [第3篇 粒子控制] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/lz07_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/lz07_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第3篇 粒子控制 - 第3篇 粒子控制 - 第3篇 粒子控制\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_步骤02 [第3篇 粒子控制] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/lz07_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/lz07_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第3篇 粒子控制 - 第3篇 粒子控制 - 第3篇 粒子控制\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="03_步骤03 [第3篇 粒子控制] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/lz07_03.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/lz07_03.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第3篇 粒子控制 - 第3篇 粒子控制 - 第3篇 粒子控制\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_步骤01 [第3篇 粒子控制] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/lz08_01.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/lz08_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第3篇 粒子控制 - 第3篇 粒子控制 - 第3篇 粒子控制\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_步骤02 [第3篇 粒子控制] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/lz08_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/lz08_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第4篇 插件程序 - 第4篇 插件程序 - 第4篇 插件程序\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_折扇生成工具 [第4篇 插件程序] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/cj01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/cj01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第4篇 插件程序 - 第4篇 插件程序 - 第4篇 插件程序\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_步骤01 [第4篇 插件程序] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/cj02_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/cj02_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第4篇 插件程序 - 第4篇 插件程序 - 第4篇 插件程序\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_步骤02 [第4篇 插件程序] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/cj02_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/cj02_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第4篇 插件程序 - 第4篇 插件程序 - 第4篇 插件程序\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="03_步骤03 [第4篇 插件程序] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/cj02_03.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/cj02_03.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第4篇 插件程序 - 第4篇 插件程序 - 第4篇 插件程序\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="04_步骤04 [第4篇 插件程序] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/cj02_04.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/cj02_04.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第4篇 插件程序 - 第4篇 插件程序 - 第4篇 插件程序\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_步骤01 [第4篇 插件程序] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/cj03_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/cj03_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第4篇 插件程序 - 第4篇 插件程序 - 第4篇 插件程序\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_步骤02 [第4篇 插件程序] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/cj03_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/cj03_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第4篇 插件程序 - 第4篇 插件程序 - 第4篇 插件程序\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="03_步骤03 [第4篇 插件程序] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/cj03_03.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/cj03_03.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第4篇 插件程序 - 第4篇 插件程序 - 第4篇 插件程序\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_功能简介01 [第4篇 插件程序] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/cj04_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/cj04_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第4篇 插件程序 - 第4篇 插件程序 - 第4篇 插件程序\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_功能简介02 [第4篇 插件程序] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/cj04_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/cj04_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第4篇 插件程序 - 第4篇 插件程序 - 第4篇 插件程序\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="03_功能简介03 [第4篇 插件程序] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/cj04_03.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/cj04_03.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第4篇 插件程序 - 第4篇 插件程序 - 第4篇 插件程序\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="04_步骤01 [第4篇 插件程序] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/cj04_04.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/cj04_04.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第4篇 插件程序 - 第4篇 插件程序 - 第4篇 插件程序\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="05_步骤02 [第4篇 插件程序] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/cj04_05.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/cj04_05.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第4篇 插件程序 - 第4篇 插件程序 - 第4篇 插件程序\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="06_步骤03 [第4篇 插件程序] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/cj04_06.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/cj04_06.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第4篇 插件程序 - 第4篇 插件程序 - 第4篇 插件程序\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="07_步骤04 [第4篇 插件程序] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/cj04_07.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/cj04_07.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第4篇 插件程序 - 第4篇 插件程序 - 第4篇 插件程序\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="08_步骤05 [第4篇 插件程序] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/cj04_08.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/cj04_08.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第4篇 插件程序 - 第4篇 插件程序 - 第4篇 插件程序\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_步骤01 [第4篇 插件程序] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/cj05_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/cj05_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第4篇 插件程序 - 第4篇 插件程序 - 第4篇 插件程序\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_步骤02 [第4篇 插件程序] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/cj05_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/cj05_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第4篇 插件程序 - 第4篇 插件程序 - 第4篇 插件程序\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="03_步骤03 [第4篇 插件程序] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/cj05_03.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/cj05_03.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第4篇 插件程序 - 第4篇 插件程序 - 第4篇 插件程序\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="04_步骤04 [第4篇 插件程序] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/cj05_04.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/cj05_04.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第4篇 插件程序 - 第4篇 插件程序 - 第4篇 插件程序\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="05_步骤05 [第4篇 插件程序] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/cj05_05.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/cj05_05.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——脚本应用篇》\n第4篇 插件程序 - 第4篇 插件程序 - 第4篇 插件程序\n本书主要讲解了3ds Max脚本系统的特效案例应用。从最基础的脚本知识进行分析入手，采用简单易学的学习形式，将枯燥的计算机语言类案例，讲解的生动易于记忆。同时安排了21个效果卓越的案例。案例有基础和行业应用之分，通过案例的学习，不但可以掌握脚本的应用知识，同时可以学习脚本在电影电视特效，影视栏目包装的应用案例和技巧。\n\nhttp://got.notsh.it/3ds-max-scripts" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="06_步骤06 [第4篇 插件程序] [3ds Max 脚本应用]" --description="$(< /dev/stdin)" "DVD2/right/video/cj05_06.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/cj05_06.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafICFiG3sn38BYtoik2ODzXpb $(< /dev/stdin)

sleep 5;

